using UnityEngine;
using UnityEngine.InputSystem;

namespace Scripts
{
    public class InputControllerScript : Singleton<InputControllerScript>
    {
        #region Inspector
        [SerializeField] private Transform _rightScreenBorder;
        [SerializeField] private Transform _leftScreenBorder;
        [SerializeField] private bool _enableManualInput;
        #endregion

        #region Vars
        private MediatorScript _med;
        private InputAction _rightAction;
        private InputAction _leftAction;
        #endregion

        #region Properties
        public Transform RightBorder { get => _rightScreenBorder; set => _rightScreenBorder = value; }
        public Transform LeftBorder { get => _leftScreenBorder; set => _leftScreenBorder = value; }
        public InputAction RightAction { get => _rightAction; set => _rightAction = value; }
        public InputAction LeftAction { get => _leftAction; set => _leftAction = value; }
        public bool EnableManualInput { get => _enableManualInput; set => _enableManualInput = value; }
        #endregion

        new void Awake()
        {
            base.Awake();
            _med = GetComponentInParent<MediatorScript>();

            RightAction = new InputAction();
            RightAction.AddBinding(Keyboard.current.rightArrowKey);
            RightAction.AddBinding(Keyboard.current.dKey);
            RightAction.Enable();

            LeftAction = new InputAction();
            LeftAction.AddBinding(Keyboard.current.leftArrowKey);
            LeftAction.AddBinding(Keyboard.current.aKey);
            LeftAction.Enable();
        }

        public void InputControl()
        {
            if(EnableManualInput)
            {
                _med.AutoPlayerMovementScript.enabled = false;
                _med.PatrolDistanceRoutineScript.enabled = false;
                _med.PlayerMovementScript.enabled = true;
                _med.PlayerAnimationScript.enabled = true;
                _med.PatrolDistanceRoutineScript.StopCurrentRoutine();
            }
            else
            {
                _med.AutoPlayerMovementScript.enabled = true;
                _med.PatrolDistanceRoutineScript.enabled = true;
                _med.PlayerMovementScript.enabled = false;
                _med.PatrolDistanceRoutineScript.RestartRoutine();
            }
        }
    }
}

