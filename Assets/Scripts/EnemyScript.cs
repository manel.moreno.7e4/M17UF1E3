using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Scripts
{
    public class EnemyScript : MonoBehaviour
    {
        [SerializeField] private float _enemySpeed = 3;
        private MediatorScript _med;

        public MediatorScript Med { get => _med; set => _med = value; }

        private void FixedUpdate()
        {
            FlyingAnimationRoutine();
        }

        private void Update()
        {
            if(Med == null)
            {
                return;
            }

            FollowPlayer();
        }

        private void FollowPlayer()
        {
            var step = _enemySpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, Med.PlayerAnimationScript.gameObject.transform.position, step);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("PlayerSprite"))
            {
                _med.GetComponent<GameManagerScript>().SubstractHp();
                Destroy(gameObject);
            }
        }

        private void FlyingAnimationRoutine()
        {
            if (transform.position.y < 1.5f)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + 0.1f, 1);
            }
        }
    }
}