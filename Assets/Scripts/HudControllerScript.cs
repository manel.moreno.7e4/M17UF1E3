using UnityEngine;
using UnityEngine.UI;

namespace Scripts
{
    public class HudControllerScript : Singleton<HudControllerScript>
    {
        #region Inspector
        [SerializeField] private Text _framesToNextAnimText;
        [SerializeField] private Text _speedText;
        [SerializeField] private Text _patrolDistanceText;
        [SerializeField] private Text _currentDistanceText;
        [SerializeField] private Text _skillTimeDurationText;
        [SerializeField] private Text _cooldownTimeDurationText;
        [SerializeField] private Text _stopTimeAfterPatrol;
        [SerializeField] private Text _enableManualInputButtonText;
        [SerializeField] private Text _inputText;
        [SerializeField] private Text _playerNameText;
        [SerializeField] private Text _playerTypeText;
        [SerializeField] private Text _playerLifesText;
        [SerializeField] private Text _skillOnCooldownText;
        [SerializeField] private Text _skillNotWorkingText;
        [SerializeField] private Text _playerHeightText;
        [SerializeField] private Text _playerScoreText;
        [SerializeField] private Text _enemiesCountText;
        #endregion
        #region Vars/Properties
        private MediatorScript _med;
        public Text CurrentDistanceText { get => _currentDistanceText; set => _currentDistanceText = value; }
        public Text PlayerLifesText { get => _playerLifesText; set => _playerLifesText = value; }
        public Text SkillOnCooldownText { get => _skillOnCooldownText; set => _skillOnCooldownText = value; }
        public Text SkillNotWorkingText { get => _skillNotWorkingText; set => _skillNotWorkingText = value; }
        public Text StopTimeAfterPatrol { get => _stopTimeAfterPatrol; set => _stopTimeAfterPatrol = value; }
        public Text SkillTimeDurationText { get => _skillTimeDurationText; set => _skillTimeDurationText = value; }
        public Text CooldownTimeDurationText { get => _cooldownTimeDurationText; set => _cooldownTimeDurationText = value; }
        public Text PlayerScoreText { get => _playerScoreText; set => _playerScoreText = value; }
        #endregion
        private void Start()
        {
            _med = GetComponentInParent<MediatorScript>();
            _framesToNextAnimText.text = _med.PlayerAnimationScript.FramesToNextAnim.ToString();
            _speedText.text = _med.PlayerDataScript.Speed.ToString();
            _patrolDistanceText.text = _med.PlayerDataScript.PatrolDistance.ToString();
            StopTimeAfterPatrol.text = _med.PatrolDistanceRoutineScript.StopTime.ToString();
            SkillTimeDurationText.text = _med.HugeSkillPlayerScript.SkillTimeDuration.ToString();
            CooldownTimeDurationText.text = _med.HugeSkillPlayerScript.SkillCoolDown.ToString();
            _playerNameText.text = _med.PlayerDataScript.PlayerName;
            _playerTypeText.text = _med.PlayerDataScript.PlayerType.ToString();
            PlayerLifesText.text = _med.PlayerDataScript.PlayerLifes.ToString();
            _playerHeightText.text = _med.PlayerDataScript.Height.ToString();
        }

        private void Update()
        {
            PlayerNameTagPosition();
            EnemiesCountControl();
        }

        public void SumNxtAnim()
        {
            if(_med.PlayerAnimationScript.FramesToNextAnim <= 190 )
            {
                _med.PlayerAnimationScript.FramesToNextAnim += 10;
                _framesToNextAnimText.text = _med.PlayerAnimationScript.FramesToNextAnim.ToString();
            }
        }

        public void SubstractNxtAnim()
        {
            if(_med.PlayerAnimationScript.FramesToNextAnim >= 20)
            {
                _med.PlayerAnimationScript.FramesToNextAnim -= 10;
                _framesToNextAnimText.text = _med.PlayerAnimationScript.FramesToNextAnim.ToString();
            }
        }

        public void SumSpeed()
        {
            _med.PlayerDataScript.Speed++;
            _med.AutoPlayerMovementScript.Speed = _med.PlayerDataScript.Speed;
            _med.PlayerMovementScript.Speed = _med.PlayerDataScript.Speed;
            _speedText.text = _med.PlayerDataScript.Speed.ToString();
        }

        public void SubstractSpeed()
        {
            _med.PlayerDataScript.Speed--;
            _med.AutoPlayerMovementScript.Speed = _med.PlayerDataScript.Speed;
            _med.PlayerMovementScript.Speed = _med.PlayerDataScript.Speed;
            _speedText.text = _med.PlayerDataScript.Speed.ToString();
        }

        public void SumRoutineTime()
        {
            _med.PatrolDistanceRoutineScript.RoutineInitTime++;
            _patrolDistanceText.text = _med.PatrolDistanceRoutineScript.RoutineInitTime.ToString();
        }

        public void SubstractRoutineTime()
        {
            _med.PatrolDistanceRoutineScript.RoutineInitTime--;
            _patrolDistanceText.text = _med.PatrolDistanceRoutineScript.RoutineInitTime.ToString();
        }

        public void SumSkillTimeDuration()
        {
            _med.HugeSkillPlayerScript.SkillTimeDuration++;
            SkillTimeDurationText.text = _med.HugeSkillPlayerScript.SkillTimeDuration.ToString();
        }

        public void SubstractSkillTimeDuration()
        {
            _med.HugeSkillPlayerScript.SkillTimeDuration--;
            SkillTimeDurationText.text = _med.HugeSkillPlayerScript.SkillTimeDuration.ToString();
        }

        public void SumCoolDownTimeDuration()
        {
            _med.HugeSkillPlayerScript.SkillCoolDown++;
            SkillTimeDurationText.text = _med.HugeSkillPlayerScript.SkillTimeDuration.ToString();
        }

        public void SubstractCoolDownTimeDuration()
        {
            _med.HugeSkillPlayerScript.SkillCoolDown--;
            SkillTimeDurationText.text = _med.HugeSkillPlayerScript.SkillTimeDuration.ToString();
        }

        public void SumStopTimeAfterPatrol()
        {
            _med.PatrolDistanceRoutineScript.StopTime++;
            StopTimeAfterPatrol.text = _med.PatrolDistanceRoutineScript.StopTime.ToString();
        }

        public void SubstractStopTimeAfterPatrol()
        {
            _med.PatrolDistanceRoutineScript.StopTime--;
            StopTimeAfterPatrol.text = _med.PatrolDistanceRoutineScript.StopTime.ToString();
        }

        public void SumHeight()
        {
            if(_med.PlayerDataScript.Height < 12)
            {
                _med.PlayerDataScript.Height++;
                _playerHeightText.text = _med.PlayerDataScript.Height.ToString();
            }
        }

        public void SubstractHeight()
        {
            if (_med.PlayerDataScript.Height > 6)
            {
                _med.PlayerDataScript.Height--;
                _playerHeightText.text = _med.PlayerDataScript.Height.ToString();
            }
        }

        public void EnableManualInputControl()
        {
            if(_med.InputControllerScript.EnableManualInput)
            {
                _enableManualInputButtonText.text = "O";
                _med.InputControllerScript.EnableManualInput = false;
                _inputText.enabled = false;
                _med.InputControllerScript.InputControl();
            }

            else
            {
                _enableManualInputButtonText.text = "X";
                _med.InputControllerScript.EnableManualInput = true;
                _inputText.enabled = true;
                _med.InputControllerScript.InputControl();
            }
        }

        private void PlayerNameTagPosition()
        {
            _playerNameText.gameObject.transform.position = transform.position + new Vector3(transform.position.x + 900, transform.position.y+ 300, transform.position.z);
            _playerNameText.gameObject.transform.position = _playerNameText.gameObject.transform.position + new Vector3(transform.position.x * 100, transform.position.y, transform.position.z);
        }

        private void EnemiesCountControl()
        {
            var enemiesCount = GameObject.FindGameObjectsWithTag("EnemySprite").Length;
            _enemiesCountText.text = enemiesCount.ToString();
        }

        public void OnStartGameClick()
        {
            StartCoroutine(_med.GetComponent<GameManagerScript>().ActivateEnemySpawner());
        }
    }
}