using UnityEngine;

namespace Scripts 
{
    public class MediatorScript : Singleton<MediatorScript>
    {
        #region Vars
        private PlayerAnimationScript _playerAnimationScript;
        private PatrolDistanceRoutineScript _patrolRoutineScript;
        private AutoPlayerMovementScript _patrolScript;
        private PlayerDataScript _playerDataScript;
        private InputControllerScript _inputControllerScript;
        private UserInputPlayerMovementScript _playerMovementScript;
        private HugePlayerSkillScript _hugeSkillPlayerScript;
        private HudControllerScript _hudPlayerControllerScript;
        #endregion

        #region Properties
        public PlayerAnimationScript PlayerAnimationScript { get => _playerAnimationScript; set => _playerAnimationScript = value; }
        public PatrolDistanceRoutineScript PatrolDistanceRoutineScript { get => _patrolRoutineScript; set => _patrolRoutineScript = value; }
        public AutoPlayerMovementScript AutoPlayerMovementScript { get => _patrolScript; set => _patrolScript = value; }
        public PlayerDataScript PlayerDataScript { get => _playerDataScript; set => _playerDataScript = value; }
        public InputControllerScript InputControllerScript { get => _inputControllerScript; set => _inputControllerScript = value; }
        public UserInputPlayerMovementScript PlayerMovementScript { get => _playerMovementScript; set => _playerMovementScript = value; }
        public HugePlayerSkillScript HugeSkillPlayerScript { get => _hugeSkillPlayerScript; set => _hugeSkillPlayerScript = value; }
        public HudControllerScript HudControllerScript { get => _hudPlayerControllerScript; set => _hudPlayerControllerScript = value; }
        #endregion

        new void Awake()
        {
            base.Awake();
            PlayerAnimationScript = GetComponentInChildren<PlayerAnimationScript>();
            PatrolDistanceRoutineScript = GetComponentInChildren<PatrolDistanceRoutineScript>();
            AutoPlayerMovementScript = GetComponentInChildren<AutoPlayerMovementScript>();
            PlayerDataScript = GetComponentInChildren<PlayerDataScript>();
            InputControllerScript = GetComponentInChildren<InputControllerScript>();
            PlayerMovementScript = GetComponentInChildren<UserInputPlayerMovementScript>();
            HugeSkillPlayerScript = GetComponentInChildren<HugePlayerSkillScript>();
            HudControllerScript = GetComponentInChildren<HudControllerScript>();
        }
    }
}