using System.Collections;
using UnityEngine;

namespace Scripts
{
    public class PatrolDistanceRoutineScript : Singleton<PatrolDistanceRoutineScript>
    {
        #region Vars
        private MediatorScript _med;
        private float _routineInitTime;
        private float _timeCounter;
        private float _savedSpeed;
        private bool _isStop;
        private float _stopTime = 1;
        #endregion

        #region Properties
        public float RoutineInitTime { get => _routineInitTime; set => _routineInitTime = value; }
        public bool IsStop { get => _isStop; set => _isStop = value; }
        public float StopTime { get => _stopTime; set => _stopTime = value; }

        #endregion

        private void Start()
        {
            _med = GetComponentInParent<MediatorScript>();
            RoutineInitTime = _med.PlayerDataScript.PatrolDistance;
            StartCoroutine(PatrolDistanceRoutine(RoutineInitTime));
        }
        public void StopEverything()
        {
            _med.PlayerAnimationScript.enabled = false;
            _savedSpeed = _med.AutoPlayerMovementScript.Speed;
            _med.AutoPlayerMovementScript.Speed = 0;
            _med.HudControllerScript.SkillNotWorkingText.gameObject.SetActive(true);
        }

        public void EnableEverything()
        {
            _med.PlayerAnimationScript.enabled = true;
            _med.AutoPlayerMovementScript.Speed = _savedSpeed;
        }


        IEnumerator PatrolDistanceRoutine(float routineTime)
        {
            _timeCounter = routineTime;
            while (_timeCounter > 0)
            {
                yield return new WaitForSeconds(1);
                _timeCounter--;
                _med.HudControllerScript.CurrentDistanceText.text = _timeCounter.ToString();
            }
            IsStop = true;
            StopEverything();
            while(StopTime > 0)
            {
                yield return new WaitForSeconds(1);
                StopTime--;
                _med.HudControllerScript.StopTimeAfterPatrol.text = StopTime.ToString();
            }
            yield return new WaitForSeconds(1);
            _stopTime = 1;
            _med.HudControllerScript.StopTimeAfterPatrol.text = StopTime.ToString();
            IsStop = false;
            EnableEverything();
            StartCoroutine(PatrolDistanceRoutine(RoutineInitTime));
        }

        public void StopCurrentRoutine()
        {
            StopAllCoroutines();
        }

        public void RestartRoutine()
        {
            StartCoroutine(PatrolDistanceRoutine(RoutineInitTime));
        }
    }
}