using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Scripts
{
    public class GameManagerScript : Singleton<GameManagerScript>
    {
        #region Vars
        [SerializeField] private GameObject _gameOverScreenOverlay;
        private Canvas _sceneCanvas;
        private MediatorScript _med;
        private int _score;
        private InputAction _restartAction = new InputAction();

        private float _enemiesFreq = 2;
        private EnemySpawnerScript[] _enemySpawners = new EnemySpawnerScript[2];
        [SerializeField] private Text _finalScoreText;

        private bool _pressedGameStartButton;
        #endregion

        new void Awake()
        {
            base.Awake();
            _sceneCanvas = GetComponentInChildren<Canvas>();
            _restartAction.AddBinding(Keyboard.current.rKey);
            _restartAction.Enable();
            _med = GetComponent<MediatorScript>();
            _enemySpawners = GetComponentsInChildren<EnemySpawnerScript>();
        }

        private void GameOverScreen()
        {
            _gameOverScreenOverlay = Instantiate(_gameOverScreenOverlay, _sceneCanvas.transform);
            _finalScoreText = _gameOverScreenOverlay.GetComponentsInChildren<Text>()[4];
            _finalScoreText.text = _score.ToString();
            Time.timeScale = 0;
            _med.PlayerAnimationScript.enabled = false;
        }

        public void SubstractHp()
        {
            _med.PlayerDataScript.PlayerLifes -= 1;
            _med.HudControllerScript.PlayerLifesText.text = _med.PlayerDataScript.PlayerLifes.ToString();

            if(_med.PlayerDataScript.PlayerLifes < 1)
            {
                GameOverScreen();
            }
        }

        public void IncreaseScoreOnKillingEnemy()
        {
            _score += 5;
            _med.HudControllerScript.PlayerScoreText.text = _score.ToString();
        }

        private void Update()
        {
            MouseControlClick();
            RPressControl();
            if(_pressedGameStartButton)
            {
                _enemiesFreq -= 0.0001f;
            }
            
        }

        private void MouseControlClick()
        {
            var mouse = Mouse.current;
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
            RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero);
            if (hit)
            {
                if (hit.transform.gameObject.CompareTag("EnemySprite"))
                {
                    if (mouse.leftButton.isPressed)
                    {
                        IncreaseScoreOnKillingEnemy();
                        Destroy(hit.transform.gameObject);
                    }
                }
            }
        }

        public void RPressControl()
        {
            if (_gameOverScreenOverlay.activeInHierarchy && _restartAction.triggered)
            {
                SceneManager.LoadScene(0);
                Time.timeScale = 1;
            }
        }

        public IEnumerator ActivateEnemySpawner()
        {
            _pressedGameStartButton = true;
            _enemySpawners[0].SpawnEnemy1 = true;
            yield return new WaitForSeconds(_enemiesFreq);
            _enemySpawners[1].SpawnEnemy1 = true;
            yield return new WaitForSeconds(_enemiesFreq);
            StartCoroutine(ActivateEnemySpawner());
        }
    }
}

