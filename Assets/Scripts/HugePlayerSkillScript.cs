using System.Collections;
using UnityEngine;

namespace Scripts
{
    public class HugePlayerSkillScript : Singleton<HugePlayerSkillScript>
    {
        #region Inspector
        [SerializeField] private float _skillTimeDuration;
        [SerializeField] private float _skillCoolDown;
        #endregion

        #region Vars
        private float _scaleGrowFactor = 0.5f;
        private float _finalScale;
        private Vector3 _fixedChildScale = new Vector3(3.3f, 3.3f, 1);
        private Vector3 _initScale;
        private MediatorScript _med;
        private SkillStates _currentState = SkillStates.NotInit;
        private bool _execOnce;
        private bool _execOnce2;

        public float SkillTimeDuration { get => _skillTimeDuration; set => _skillTimeDuration = value; }
        public float SkillCoolDown { get => _skillCoolDown; set => _skillCoolDown = value; }
        #endregion


        new void Awake()
        {
            base.Awake();
            _med = GetComponentInParent<MediatorScript>();
            _initScale = _med.PlayerAnimationScript.ScaleToInstantiate;
            if(_initScale.x > 7.5f)
            {
                _scaleGrowFactor = 0.3f;
            }

            if (_initScale.x > 10f)
            {
                _scaleGrowFactor = 0.2f;
            }
            _finalScale = _initScale.x * _scaleGrowFactor;
        }
        public enum SkillStates
        {
            NotInit,
            Growing,
            Grown,
            Ungrow,
            Cooldown,
        }

        private void FixedUpdate()
        {
            if(_currentState != SkillStates.NotInit)
            {
                SkillStateSequence();
            }
        }

        public void InitSkill()
        {
            if(_currentState == SkillStates.NotInit)
            {
                _currentState = SkillStates.Growing;
            }

            else if(_currentState == SkillStates.Cooldown)
            {
                ShowCooldownMessage();
            }
        }

        private void ShowCooldownMessage()
        {
            _med.HudControllerScript.SkillOnCooldownText.gameObject.SetActive(true);
        }

        private void SkillStateSequence()
        {
            switch(_currentState)
            {
                case SkillStates.Growing:
                    StartCoroutine(GrowRoutine());
                    break;
                case SkillStates.Grown:
                    StartCoroutine(SkillTimeControlRoutine());
                    break;
                case SkillStates.Ungrow:
                    StartCoroutine(UnGrowRoutine());
                    break;
                case SkillStates.Cooldown:
                    StartCoroutine(CoolDownControlRoutine());
                    break;
            }
        }


        IEnumerator CoolDownControlRoutine()
        {
            if(!_execOnce)
            {
                _execOnce = true;
                var initCoolDown = SkillCoolDown;
                while (SkillCoolDown > 0)
                {
                    yield return new WaitForSeconds(1);
                    SkillCoolDown--;
                    _med.HudControllerScript.CooldownTimeDurationText.text = SkillCoolDown.ToString();
                }
                yield return new WaitForSeconds(1);
                SkillCoolDown = initCoolDown;
                _med.HudControllerScript.CooldownTimeDurationText.text = SkillCoolDown.ToString();
                _currentState = SkillStates.NotInit;
                SetDefaults();
            }
        }

        IEnumerator SkillTimeControlRoutine()
        {
            if(!_execOnce2)
            {
                _execOnce2 = true;
                var initSkillTimeDuration = SkillTimeDuration;
                while (SkillTimeDuration > 0)
                {
                    yield return new WaitForSeconds(1);
                    SkillTimeDuration--;
                    _med.HudControllerScript.SkillTimeDurationText.text = SkillTimeDuration.ToString();
                }
                _currentState = SkillStates.Ungrow;
                yield return new WaitForSeconds(1);
                SkillTimeDuration = initSkillTimeDuration;
                _med.HudControllerScript.SkillTimeDurationText.text = SkillTimeDuration.ToString();
            }
        }

        IEnumerator GrowRoutine()
        {
            if (GetComponentInChildren<Transform>().localScale.x < _finalScale)
            {
                if(!_med.PatrolDistanceRoutineScript.IsStop)
                {
                    GetComponentInChildren<Transform>().localScale = new Vector3(GetComponentInChildren<Transform>().localScale.x + 0.1f, GetComponentInChildren<Transform>().localScale.y + 0.1f, 1);
                }
            }
            else 
            {
                _currentState = SkillStates.Grown;
            }
            yield return new WaitForSeconds(0.5f);
        }

        IEnumerator UnGrowRoutine()
        {
            if (GetComponentInChildren<Transform>().localScale.x > _fixedChildScale.x)
            {
                if (!_med.PatrolDistanceRoutineScript.IsStop)
                {
                    GetComponentInChildren<Transform>().localScale = new Vector3(GetComponentInChildren<Transform>().localScale.x - 0.1f, GetComponentInChildren<Transform>().localScale.y - 0.1f, 1);
                }
            }

            else if(transform.localScale.x > 1)
            {
                if (!_med.PatrolDistanceRoutineScript.IsStop)
                {
                    transform.localScale = new Vector3(transform.localScale.x - 0.1f, transform.localScale.y - 0.1f, 1);
                }
            }
            else
            {
                _currentState = SkillStates.Cooldown;
            }
            yield return new WaitForSeconds(0.5f);
        }
        private void SetDefaults()
        {
            _execOnce = false;
            _execOnce2 = false;
        }
    }
}
