using UnityEngine;
using UnityEngine.UI;

namespace Scripts
{
    public class TotalFramesControlScript : MonoBehaviour
    {
        private float _totalFramesCount = 0;
        private Text _totalFramesText;

        private void Awake()
        {
            _totalFramesText = GetComponent<Text>();
        }

        private void Update()
        {
            _totalFramesCount++;
            _totalFramesText.text = _totalFramesCount.ToString();
        }
    }
}