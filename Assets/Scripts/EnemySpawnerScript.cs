using System.Collections;
using UnityEngine;

namespace Scripts
{
    public class EnemySpawnerScript : MonoBehaviour
    {
        #region Inspector
        [SerializeField] private GameObject _enemyChickenPrefab;
        [SerializeField] private bool _spawnEnemy;
        [SerializeField] private bool _deleteAllenemies;
        [SerializeField] private float _enemiesSpawnCooldown;
        #endregion
        #region Vars
        private bool _execOnce;
        private MediatorScript _med;

        public bool SpawnEnemy1 { get => _spawnEnemy; set => _spawnEnemy = value; }
        #endregion


        private void Awake()
        {
            _med = GetComponentInParent<MediatorScript>();
        }

        private void Update()
        {
            if(SpawnEnemy1)
            {
                SpawnEnemy();
            }

            if(_deleteAllenemies)
            {
                DeleteAllEnemies();
            }
        }
        private void SpawnEnemy()
        {
            if(!_execOnce)
            {
                _execOnce = true;
                var currentEnemy = Instantiate(_enemyChickenPrefab, transform.position, Quaternion.identity);
                currentEnemy.transform.parent = gameObject.transform;
                currentEnemy.GetComponent<EnemyScript>().Med = _med;
                StartCoroutine(ExecOnceToFalse());
            }
        }

        private void DeleteAllEnemies()
        {
            if(GetComponentsInChildren<Transform>().Length > 0)
            {
                foreach(var enemy in GetComponentsInChildren<Transform>())
                {
                    if(enemy != GetComponentsInChildren<Transform>()[0])
                    {
                        Destroy(enemy.gameObject);
                    }
                }
                _deleteAllenemies = false;
            }
        }

        IEnumerator ExecOnceToFalse()
        {
            yield return new WaitForSeconds(_enemiesSpawnCooldown);
            SpawnEnemy1 = false;
            _execOnce = false;
        }
    }
}

