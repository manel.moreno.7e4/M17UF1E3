using UnityEngine;

namespace Scripts
{
    public class UserInputPlayerMovementScript : Singleton<UserInputPlayerMovementScript>
    {
        #region Vars
        private MediatorScript _med;
        private float _speed;
        public float Speed { get => _speed; set => _speed = value; }
        #endregion

        new void Awake()
        {
            base.Awake();
            _med = GetComponentInParent<MediatorScript>();
        }
        private void OnEnable()
        {
            Speed = _med.PlayerDataScript.Speed;
        }
        private void Update()
        {
            MovementInputDriven();
        }

        private void MovementInputDriven()
        {
            if (_med.InputControllerScript.RightAction.ReadValue<float>() > 0)
            {
                if (transform.position.x < _med.InputControllerScript.RightBorder.position.x)
                {
                    transform.position = new Vector3(transform.position.x + Speed * Time.deltaTime, transform.position.y, transform.position.z);
                    _med.AutoPlayerMovementScript.GoingRight = true;
                }
            }

            if (_med.InputControllerScript.LeftAction.ReadValue<float>() > 0)
            {
                if (transform.position.x > _med.InputControllerScript.LeftBorder.position.x)
                {
                    transform.position = new Vector3(transform.position.x - Speed * Time.deltaTime, transform.position.y, transform.position.z);
                    _med.AutoPlayerMovementScript.GoingRight = false;
                }
            }
        }
    }
}

