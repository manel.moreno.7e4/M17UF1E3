using UnityEngine;

namespace Scripts
{
    public class PlayerAnimationScript : Singleton<PlayerAnimationScript>
    {
        #region Inspector
        [SerializeField] private GameObject player1;
        [SerializeField] private GameObject player2;
        [SerializeField] private GameObject player3;
        [SerializeField] private byte framesToNextAnim = 60;
        #endregion

        #region Vars
        private GameObject _currentGameObject;
        private MediatorScript _med;
        private PlayerAnimationStates _currentState = PlayerAnimationStates.player3;
        private byte frameCount;
        private Vector3 lastPosition = Vector3.zero;
        private Vector3 _scaleToInstantiate;
        public Vector3 ScaleToInstantiate { get => _scaleToInstantiate; set => _scaleToInstantiate = value; }
        public byte FramesToNextAnim { get => framesToNextAnim; set => framesToNextAnim = value; }
        #endregion

        public enum PlayerAnimationStates
        {
            player1 = 1,
            player2 = 2,
            player3 = 3
        }

        new void Awake()
        {
            base.Awake();
            _med = GetComponentInParent<MediatorScript>();
            ScaleToInstantiate = new Vector3(_med.PlayerDataScript.Height, _med.PlayerDataScript.Height, 1);
        }
        void Update()
        {
            FrameCounter();
            CheckSpriteDirection();
            UpdateScaleOnRuntime();
        }

        private void FrameCounter()
        {
            frameCount++;
            if(frameCount > FramesToNextAnim)
            {
                InstantiateNextImage();
                frameCount = 0;
            }
        }

        private void InstantiateNextImage()
        {
            DeleteCurrentImage();
            switch (_currentState)
            {
                case PlayerAnimationStates.player1:
                    _currentState = PlayerAnimationStates.player2;
                    _currentGameObject = Instantiate(player2, lastPosition, Quaternion.identity);
                    break;
                case PlayerAnimationStates.player2:
                    _currentState = PlayerAnimationStates.player3;
                    _currentGameObject = Instantiate(player3, lastPosition, Quaternion.identity);
                    break;
                case PlayerAnimationStates.player3:
                    _currentState = PlayerAnimationStates.player1;
                    _currentGameObject = Instantiate(player1, lastPosition, Quaternion.identity);
                    break;
            }
            FixChildrenTransform();
        }

        private void FixChildrenTransform()
        {
            _currentGameObject.transform.parent = gameObject.transform;
            _currentGameObject.transform.position = new Vector3(transform.position.x, _currentGameObject.transform.position.y, 1);
            _currentGameObject.transform.localScale = _scaleToInstantiate;
        }

        private void DeleteCurrentImage()
        {
            foreach(Transform image in GetComponentInChildren<Transform>())
            {
                lastPosition = image.transform.position;
                Destroy(image.gameObject);
            }
        }

        private void CheckSpriteDirection()
        {
            if(_med.AutoPlayerMovementScript.GoingRight && _currentGameObject != null)
            {
                _currentGameObject.GetComponent<SpriteRenderer>().flipX = true;
            }
        }

        private void UpdateScaleOnRuntime()
        {
            if(_currentGameObject != null)
            {
                ScaleToInstantiate = new Vector3(_med.PlayerDataScript.Height, _med.PlayerDataScript.Height, 1);
                _currentGameObject.transform.localScale = _scaleToInstantiate;
            }
        }
    }
}