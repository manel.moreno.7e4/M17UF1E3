using UnityEngine;


namespace Scripts
{
    public class PlayerDataScript : Singleton<PlayerDataScript>
    {
        #region Inspector
        [SerializeField] private string _playerName = "hehe";
        [SerializeField] private string _playerSurName = "xd";
        [SerializeField] private PlayerTypes _playerType;
        [Range(6f, 12f)]
        [SerializeField] private float _height;
        [SerializeField] private float _weight;
        [SerializeField] private float _speed;
        [SerializeField] private float _patrolDistance;
        [SerializeField] private float _weightSpeedFactor;
        [SerializeField] private byte _playerLifes;
        private int _score;
        #endregion

        #region Properties
        public string PlayerName { get => _playerName; set => _playerName = value; }
        public string PlayerSurName { get => _playerSurName; set => _playerSurName = value; }

        public float Height { get => _height; set => _height = value; }
        public float Speed { get => _speed; set => _speed = value; }
        public float PatrolDistance { get => _patrolDistance; set => _patrolDistance = value; }
        public PlayerTypes PlayerType { get => _playerType; set => _playerType = value; }
        public byte PlayerLifes { get => _playerLifes; set => _playerLifes = value; }
        public int Score { get => _score; set => _score = value; }
        #endregion

        public enum PlayerTypes
        {
            WalkingGirl,
            AttackingGirl,
            DefendingGirl
        }
        new void Awake()
        {
            base.Awake();
            _speed = CalculateSpeed();
        }

        private float CalculateSpeed()
        {
            if(_weight <= 0)
            {
                return _speed;
            }

            else
            {
                return _speed * _weightSpeedFactor / _weight;
            }
        }
    }
}