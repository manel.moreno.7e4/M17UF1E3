using UnityEngine;


namespace Scripts
{
    public class AutoPlayerMovementScript : Singleton<AutoPlayerMovementScript>
    {
        #region Vars
        private float speed;
        public bool GoingRight { get => goingRight; set => goingRight = value; }
        public float Speed { get => speed; set => speed = value; }
        private bool goingRight = true;
        private MediatorScript _med;
        #endregion
        new void Awake()
        {
            base.Awake();
            _med = GetComponentInParent<MediatorScript>();
        }

        private void Update()
        {
            AutoPatrolMovement();
        }

        private void Start()
        {
            Speed = _med.PlayerDataScript.Speed;
        }

        private void AutoPatrolMovement()
        {
            if (GoingRight)
            {
                if (transform.position.x < _med.InputControllerScript.RightBorder.position.x)
                {
                    transform.position = new Vector3(transform.position.x + Speed * Time.deltaTime, transform.position.y, transform.position.z);
                }

                else
                {
                    GoingRight = false;
                }
            }

            else
            {
                if (transform.position.x > _med.InputControllerScript.LeftBorder.position.x)
                {
                    transform.position = new Vector3(transform.position.x - Speed * Time.deltaTime, transform.position.y, transform.position.z);
                }

                else
                {
                    GoingRight = true;
                }
            }
        }
    }
}

