using UnityEngine;

public class AutoDisableObjectScript : MonoBehaviour
{
    public void DisableObject()
    {
        gameObject.SetActive(false);
    }
}
